React is a declarative, efficient, and flexible JavaScript library for building user interfaces.
components tell React what you want to render.
React will efficiently update and render just the right components when your data changes
A component takes in parameters, called props
returns a hierarchy of views to display via the render method.
render returns a React element, which is a lightweight description of what to render
<div /> syntax is transformed at build time to React.createElement('div')
 React developers use a special syntax called JSX which makes it easier to write react elements 
