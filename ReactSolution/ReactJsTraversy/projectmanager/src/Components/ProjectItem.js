import React, { Component } from 'react';



class ProjectItem extends Component {
  deleteproject(id){
    console.log('Deleting Project');
    
    this.props.onDelete(id);
  }
    render() {
      console.log(this.props)
        return (
            <li className="ProjectItem">
                {this.props.projectItem.title} - {this.props.projectItem.category} < a href="#" onClick={this.deleteproject.bind(this,this.props.projectItem.id)}>X</a>
            </li>
        );
    }
}

export default ProjectItem;
