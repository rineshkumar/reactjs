import React, { Component } from 'react';

import $ from 'jquery'
import  Projects from './Components/Projects'
import AddProject from './Components/AddProject'
import Todos from './Components/Todos'
import uuid from 'uuid'
class App extends Component {
  constructor(){
    super()
    this.state = {
      projects: [],
      todos : []
    }
  }
  getProjects(){
    this.setState({
      projects : [
        {
          id:uuid.v4(),
          title : 'Business web site ' ,
          category : 'Web design'
        },
        {
          id:uuid.v4(),
          title : 'Social App ' ,
          category : 'Mobile Development'
        },
        {
          id:uuid.v4(),
          title : 'Ecommerce Shopping cart ' ,
          category : 'Web Development'
        }
      ]
    });
  }
  getTodos(){
  $.ajax({
     url:"https://jsonplaceholder.typicode.com/todos",
     dataType: 'json',
     cache: false,
     success: function(data) {

       this.setState({todos: data});
     }.bind(this),
     error: function(xhr, status, err) {
       console.error(this.props.url, status, err.toString());
     }.bind(this)
   });


  }
componentDidMount(){
  this.getTodos();
  this.getProjects();
}

  componentWillMount(){
    this.getTodos();
    this.getProjects();
  }
  handleAddProject(project){
    let projects = this.state.projects;
    projects.push(project);
    this.setState({projects:projects})
  }
  handleDeleteProject(id){

    let projects = this.state.projects;
    let index = projects.findIndex(x => x.id === id);
    projects.splice(index,1);
    this.setState({projects:projects})
  }
  render() {
    return (
      <div className="App">
        <AddProject addProject={this.handleAddProject.bind(this)}/>
        <Projects projects={this.state.projects} onDelete={this.handleDeleteProject.bind(this)}/>
        <Todos todos={this.state.todos}/>
      </div>
    );
  }
}

export default App;
