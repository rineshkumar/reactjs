import React, { Component } from 'react';
import uuid from 'uuid'


class AddProject extends Component {
  constructor(){
    super();
    this.state={
      newProject:{}
    }
  }
  static defaultProps = {
    categories : ['Web Design','Web Development', 'Mobile Development']
  }
  handleSubmit(e){

    if(this.refs.title.value === '' ){
      alert("Title required.");
    }else{
      this.setState({
        newProject :{
          id : uuid.v4(),
          title : this.refs.title.value,
          category: this.refs.category.value,
        }
      },function(){
        console.log(this.state);

        this.props.addProject(this.state.newProject)
      });
    }
    console.log("Submitted");
    //console.log(this.refs.title.value);
    e.preventDefault();

  }
    render() {
      let categoryOptions = this.props.categories.map(c => {
        return <option key={c} value={c}>{c}</option>
      } )
        return (
            <div className="AddProject">
                <h3>Add Project</h3>
                <form onSubmit={this.handleSubmit.bind(this)}>

                  <div>
                    <label>Title</label>
                    <input type="text" ref="title"/>
                  </div>
                  <div>
                    <label>Category</label>
                    <select ref="category">
                    {categoryOptions}
                    </select>
                  </div>
                  <input type="submit" value="Submit"/>
                </form>
            </div>
        );
    }
}

export default AddProject;
