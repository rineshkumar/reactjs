import React, { Component } from 'react';
import ProjectItem from './ProjectItem';
import PropTypes from 'prop-types'

class Projects extends Component {
  deleteproject(id){
    this.props.onDelete(id)
  }
    render() {
      let projectItems;
      if(this.props.projects){
        projectItems = this.props.projects.map(
          p => {return(<ProjectItem key={p.title} projectItem={p} onDelete={this.deleteproject.bind(this)}/>)}
        );
      }
        return (
            <div className="Projects">
                {projectItems}
            </div>
        );
    }
}
Projects.propTypes = {

  projects : PropTypes.array,
  onDelete : PropTypes.func
}
export default Projects;
